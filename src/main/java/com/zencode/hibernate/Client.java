package com.zencode.hibernate;

import com.zencode.hibernate.data.Instructor;
import com.zencode.hibernate.data.InstructorDetail;
import com.zencode.hibernate.services.InstructorService;

public class Client {
    public static void main(String[] args) {
        Instructor instructor = new Instructor("Chad", "Darby", "darby@zencode.co.za");
        InstructorDetail instructorDetail =
                new InstructorDetail("http://www.zencode.com/youtube", "Meditated code");

        Instructor instructor2 = new Instructor("Brandy", "Frasier", "brandy@zencode.co.za");
        InstructorDetail instructorDetail2 =
                new InstructorDetail("http://www.zencode.com/youtube", "Balls to the code");



        InstructorService.add(instructor, instructorDetail);
        InstructorService.add(instructor2, instructorDetail2);
        InstructorService.delete(7);
    }
}
