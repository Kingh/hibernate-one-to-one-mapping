package com.zencode.hibernate.services;

import com.zencode.hibernate.data.Instructor;
import com.zencode.hibernate.data.InstructorDetail;
import com.zencode.hibernate.session.factory.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class InstructorService {
    public static void add(Instructor instructor, InstructorDetail instructorDetail) {
        Transaction transaction;

        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            instructor.setInstructorDetail(instructorDetail);

            transaction = session.beginTransaction();

            session.save(instructor);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Instructor getInstructor(int id) {
        Transaction transaction;

        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            transaction = session.beginTransaction();

            Instructor instructor = session.get(Instructor.class, id);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void delete(int id) {
        Transaction transaction;

        try (Session session = HibernateUtil.getSessionFactory().getCurrentSession()) {
            transaction = session.beginTransaction();

            Instructor instructor = session.get(Instructor.class, id);

            if (instructor != null) {
                session.delete(instructor);
                transaction.commit();
            }
        }
    }
}
